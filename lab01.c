/*
Felipe Parra
rut: 18.847.719-4
mail: Fparracas@gmail.com
gitlab del proyecto: https://gitlab.com/fparracas/key-schedule-in-c

Laboratorio 01 de Criptografía
Enunciado: Implementar el algoritmo Key Shedule de DES en lenguaje C.
Compilado con g++ en MacOs Sierra
Computador utilizado: Macbook Air core i5, con 8gb de memoria ram y un disco ssd de 128gb.
*/

//Librería para interacción con el usuario
#include <stdio.h>

//Librería necesaria para la creacion de los numeros aleatorios
#include <time.h> 		
#include <stdlib.h>


//Declaración de variables
void menu();
void permutacionInicial(int keyIn[64], int key56[56]);
void permutacionFinal(int keyIn[56], int keyFinal[48]);
void armarKeyIzquierda(int keyIn[56], int keyIzq[28]);
void armarKeyDerecha(int keyIn[56], int keyDer[28]);
void sumarMitades(int keyMitadIzquierda[28], int keyMitadDerecha[28], int key56[56]);
void rotacion1(int keyMitad[28]);
void rotacion2(int keyMitad[28]);
void mostrarKey64(int keyIn[64]);
void mostrarKey56(int keyIn[56]);
void mostrarKey48(int keyIn[48]);
void mostrarKey28(int keyIn[28]);

//Comienzo función main
int main(int argc, char *argv[])
{
	//interaccion con el usuario
	int opcionMenu;
	int i;
	int numIn;
	menu();
	scanf("%d", &opcionMenu);

	//variable que tendrá la clave ingresada o generada.
	int keyIn[64];
	//variable que será la resultante de la primera permutacion e irá guardando la suma luego de las rotaciones
	int key56[56];

	//Segun la opcion ingresada, se ubican los valores dentro del arreglo que tendrá la key.
	switch(opcionMenu)
	{
		case 1:
			//Caso en que el usuario ingresa manualmente la clave
			printf("Ingrese los valores para la clave, recuerde que solo se pueden ingresar '0' o '1'.\n");
			for(i = 1; i <=64; ++i)
			{
				//Por 64 ciclos, que representa cada término que tendrá la clave.
				printf("Ingrese el '%d' termino: \n", i);
				scanf("%d", &numIn);

				while(numIn != 0 && numIn != 1)
				{ 
					//El siguiente while confirma que el numero ingresado sea solo 0's y 1's
					printf("Recuerde que el numero solo puede ser un '0' o '1', reingrese el número\n");
					scanf("%d", &numIn); 
				}
				keyIn[i] = numIn;
			}

			//Mostrar en pantalla
			printf("La Key ingresada recién sería la siguiente:\n");
			mostrarKey64(keyIn);
		break;

		case 2:
			//Declaración necesaria para crear el número aleatorio
			srand(time(NULL));	 	
			for(i = 1; i <= 64; ++i)
			{
				//por cada espacio del array, sin contar el espacio 0, se le da un valor aleatorio entre 1 y 0.
				keyIn[i] = rand() % 2;
			}
			printf("Key generada aleatoriamente:\n");
			mostrarKey64(keyIn);
			printf("\n");
		break;
	}

	//Se realiza la primera permutacion
	permutacionInicial(keyIn, key56);
	printf("Primera permutacion (PC-1):\n");
	mostrarKey56(key56);
	printf("\n");
	//Variables que servirán para las mitades C0 y D0.
	int keyMitadIzquierda[28];
	int keyMitadDerecha[28];

	//La concatenación se irá guardando en el key de 56.
	//Variable que servirá para mostrar las 16 keys generadas.
	int keyGenerada[48];

	//Comienzo de los 16 ciclos restantes.
	for(i = 1; i<= 16; ++i)
	{
		//Por cada ciclo, se divide en 2 mitades la primera permutacion
		armarKeyIzquierda(key56, keyMitadIzquierda);
		armarKeyDerecha(key56, keyMitadDerecha);

		printf("\nKey de 56 de bits que se dividira en dos:\n");
		mostrarKey56(key56);
		printf("\nMitad izquierda:\n");
		mostrarKey28(keyMitadIzquierda);
		printf("\nMitad derecha:\n");
		mostrarKey28(keyMitadDerecha);

		//Se crean las 2 condiciones, si el número del ciclo es igual a 
		// 1,2,9 o 16. se hace una sola rotación, de lo contrario se hacen 2.
		if(i == 1 || i == 2 || i == 9 || i == 16)
		{
			printf("\nMitad izquierda con 1 rotacion:\n");
			rotacion1(keyMitadIzquierda);
			printf("\nMitad derecha con 1 rotacion:\n");
			rotacion1(keyMitadDerecha);
		}
		else
		{
			printf("\nMitad izquierda con 2 rotaciones:\n");
			rotacion2(keyMitadIzquierda);
			printf("\nMitad derecha con 2 rotaciones:\n");
			rotacion2(keyMitadDerecha);
		}
		//luego de hacer las rotaciones correspondientes se suman ambas mitades
		sumarMitades(keyMitadIzquierda, keyMitadDerecha, key56);

		printf("\nkey de 56 bits producto de la suma de las mitades rotadas:\n");
		mostrarKey56(key56);

		//y se hace la permutacion final con que se generará la subllave de 48 bits
		permutacionFinal(key56, keyGenerada);
		printf("\nsub-key generada numero %d:\n", i);
		mostrarKey48(keyGenerada);
		printf("\n____________________________________________________________\n\n");
	}
	return 0;
}

//Grupo de funciones mostrar para cada largo de las keys.
void mostrarKey64(int keyIn[64])
{
	int i;
	for(i = 1; i <= 64; ++i)
	{
		printf("%d", keyIn[i]);
	}
	printf("\n");	
}

void mostrarKey56(int keyIn[56])
{
	int i;
	for(i = 1; i <= 56; ++i)
	{
		printf("%d", keyIn[i]);
	}
	printf("\n");	
}

void mostrarKey48(int keyIn[48])
{
	int i;
	for(i = 1; i <= 48; ++i)
	{
		printf("%d", keyIn[i]);
	}
	printf("\n");
}

void mostrarKey28(int keyIn[28])
{
	int i;
	for(i = 1; i <= 28; ++i)
	{
		printf("%d", keyIn[i]);
	}
	printf("\n");
}


void permutacionFinal(int keyIn[56], int keyFinal[48])
{
	//se crea la nueva key con las posiciones ya establecidas.
	//como no se aprecia un patron que se pueda programar, se aplica fuerza bruta.
	keyFinal[1] = keyIn[14];
	keyFinal[2] = keyIn[17];
	keyFinal[3] = keyIn[11];
	keyFinal[4] = keyIn[24];
	keyFinal[5] = keyIn[1];
	keyFinal[6] = keyIn[5];
	keyFinal[7] = keyIn[3];
	keyFinal[8] = keyIn[28];
	keyFinal[9] = keyIn[15];
	keyFinal[10] = keyIn[6];
	keyFinal[11] = keyIn[21];
	keyFinal[12] = keyIn[10];
	keyFinal[13] = keyIn[23];
	keyFinal[14] = keyIn[19];
	keyFinal[15] = keyIn[12];
	keyFinal[16] = keyIn[4];
	keyFinal[17] = keyIn[26];
	keyFinal[18] = keyIn[8];
	keyFinal[19] = keyIn[16];
	keyFinal[20] = keyIn[7];
	keyFinal[21] = keyIn[27];
	keyFinal[22] = keyIn[20];
	keyFinal[23] = keyIn[13];
	keyFinal[24] = keyIn[2];
	keyFinal[25] = keyIn[41];
	keyFinal[26] = keyIn[52];
	keyFinal[27] = keyIn[31];
	keyFinal[28] = keyIn[37];
	keyFinal[29] = keyIn[47];
	keyFinal[30] = keyIn[55];
	keyFinal[31] = keyIn[30];
	keyFinal[32] = keyIn[40];
	keyFinal[33] = keyIn[51];
	keyFinal[34] = keyIn[45];
	keyFinal[35] = keyIn[33];
	keyFinal[36] = keyIn[48];
	keyFinal[37] = keyIn[44];
	keyFinal[38] = keyIn[49];
	keyFinal[39] = keyIn[39];
	keyFinal[40] = keyIn[56];
	keyFinal[41] = keyIn[34];
	keyFinal[42] = keyIn[53];
	keyFinal[43] = keyIn[46];
	keyFinal[44] = keyIn[42];
	keyFinal[45] = keyIn[50];
	keyFinal[46] = keyIn[36];
	keyFinal[47] = keyIn[29];
	keyFinal[48] = keyIn[32];
}

void sumarMitades(int keyMitadIzquierda[28], int keyMitadDerecha[28], int key56[56])
{
	//Simultáneamente va ubicando la mitad izquierda desde el prinpcio de la nueva key
	// y la segunda mitad la ubica desde la mitad+1 espacio.
	int i;
	for(i = 1; i <= 28; ++i)
	{
		key56[i] = keyMitadIzquierda[i];
		key56[28 + i] = keyMitadDerecha[i];
	}
}

void rotacion1(int keyMitad[28])
{
	/*printf("\nKey antes de hacerle 1 rotacion:\n");
	mostrarKey28(keyMitad);
	*/
	//Se crea una variable auxiliar que guardará el elemento que se rotará
	int aux = keyMitad[1];
	int i;
	//se mueve un espacio a la izquierda todos los elementos restantes de la key
	for(i = 2; i <= 28; ++i )
	{
		keyMitad[i - 1] = keyMitad[i];
	}
	//y se ubica al final la variable guardada en el auxiliar
	keyMitad[28] = aux;
	mostrarKey28(keyMitad);
}

void rotacion2(int keyMitad[28])
{
	int i;
	printf("\nKey antes de hacerle 2 rotacion:\n");
	mostrarKey28(keyMitad);

	//Se crean dos variables auxiliares que guardarán los elementos que se rotarán
	int aux = keyMitad[1];
	int aux2= keyMitad[2];
	//Se mueve dos espacio a la izquierda todos los elementos restantes de la key
	for(i = 3; i <= 28; ++i)
	{
		keyMitad[i - 2] = keyMitad[i];
	}
	//y se ubican al final las dos variables guardadas en las 2 auxiliares.
	keyMitad[27] = aux;
	keyMitad[28] = aux2;
	mostrarKey28(keyMitad);
}


//Funciones para dividir la key de 56 de bits, en 2 de 28.
void armarKeyIzquierda(int keyIn[56], int keyIzq[28])
{
	int i;
	for(i = 1; i<= 28; ++i)
	{
		keyIzq[i] = keyIn[i];
	}
}

void armarKeyDerecha(int keyIn[56], int keyDer[28])
{
	int i;
	for(i = 1; i<= 28; ++i)
	{
		keyDer[i] = keyIn[28 + i];
	}
}

void permutacionInicial(int keyIn[64], int key56[56])
{
		//se crea la nueva key con las posiciones ya establecidas.
		//como no se aprecia un patron que se pueda programar, se aplica fuerza bruta.
		key56[1] = keyIn[57];
		key56[2] = keyIn[49];
		key56[3] = keyIn[41];
		key56[4] = keyIn[33];
		key56[5] = keyIn[25];
		key56[6] = keyIn[17];
		key56[7] = keyIn[9];
		key56[8] = keyIn[1];
		key56[9] = keyIn[58];
		key56[10] = keyIn[50];
		key56[11] = keyIn[42];
		key56[12] = keyIn[34];
		key56[13] = keyIn[26];
		key56[14] = keyIn[18];
		key56[15] = keyIn[10];
		key56[16] = keyIn[2];
		key56[17] = keyIn[59];
		key56[18] = keyIn[51];
		key56[19] = keyIn[43];
		key56[20] = keyIn[35];
		key56[21] = keyIn[27];
		key56[22] = keyIn[19];
		key56[23] = keyIn[11];
		key56[24] = keyIn[3];
		key56[25] = keyIn[60];
		key56[26] = keyIn[52];
		key56[27] = keyIn[44];
		key56[28] = keyIn[36];
		key56[29] = keyIn[63];
		key56[30] = keyIn[55];
		key56[31] = keyIn[47];
		key56[32] = keyIn[39];
		key56[33] = keyIn[31];
		key56[34] = keyIn[23];
		key56[35] = keyIn[15];
		key56[36] = keyIn[7];
		key56[37] = keyIn[62];
		key56[38] = keyIn[54];
		key56[39] = keyIn[46];
		key56[40] = keyIn[38];
		key56[41] = keyIn[30];
		key56[42] = keyIn[22];
		key56[43] = keyIn[14];
		key56[44] = keyIn[6];
		key56[45] = keyIn[61];
		key56[46] = keyIn[53];
		key56[47] = keyIn[45];
		key56[48] = keyIn[37];
		key56[49] = keyIn[29];
		key56[50] = keyIn[21];
		key56[51] = keyIn[13];
		key56[52] = keyIn[5];
		key56[53] = keyIn[28];
		key56[54] = keyIn[20];
		key56[55] = keyIn[12];
		key56[56] = keyIn[4];
}

void menu()
{
	int opcionMenu;
	printf("Cómo ingresará la clave?\n1- Manualmente\n2- Aleatoriamente\n");
}